// Copyright (c) 2018, Philipp Matthias Schäfer (philipp.matthias.schaefer@posteo.de)
//
// This file is part of Krautschlüssel.
//
// Krautschlüssel is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Krautschlüssel is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with Kautschlüssel. If not, see <https://www.gnu.org/licenses/>.
package space.kraut.schluessel.token;

import android.app.Activity;
import android.content.Intent;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;

import java.util.Collections;

import me.dm7.barcodescanner.zxing.ZXingScannerView;


public class TokenScannerActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    public static final String SCANNED_TOKEN = "space.kraut.schluessel.token.SCANNED_TOKEN";

    private ZXingScannerView view;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        view = new ZXingScannerView(this);

        view.setFlash(false);
        view.setAutoFocus(false);
        view.setFormats(Collections.singletonList(BarcodeFormat.QR_CODE));

        setContentView(view);
    }

    @Override
    protected void onResume() {
        super.onResume();
        view.setResultHandler(this);
        view.startCamera();
    }

    @Override
    protected void onPause() {
        super.onPause();
        view.stopCamera();
    }

    @Override
    public void handleResult(Result result) {
        String token = result.getText();
        Intent resultIntent = new Intent();
        resultIntent.putExtra(SCANNED_TOKEN, token);
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }

}
