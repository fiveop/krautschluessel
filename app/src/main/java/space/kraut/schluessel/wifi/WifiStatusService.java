// Copyright (c) 2018, Philipp Matthias Schäfer (philipp.matthias.schaefer@posteo.de)
//
// This file is part of Krautschlüssel.
//
// Krautschlüssel is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Krautschlüssel is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with Kautschlüssel. If not, see <https://www.gnu.org/licenses/>.
package space.kraut.schluessel.wifi;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.annotation.NonNull;

import java.util.regex.Pattern;

import space.kraut.schluessel.shared.AbstractChangeNotifyingService;

public class WifiStatusService extends AbstractChangeNotifyingService {

    public enum WifiStatus {

        CORRECT_NETWORK,
        WRONG_NETWORK,
        NOT_CONNECTED,
        NOT_ENABLED

    }

    private static final String EXPECTED_SSID_QUOTED = "\"tuer.kraut.space\"";
    private static final Pattern QUOTED_SSID_PATTERN = Pattern.compile("^\"(.*)\"$");

    private static WifiStatusService instance;

    private WifiStatus status;

    private WifiStatusService() {}

    public static WifiStatusService getInstance() {
        if(instance == null) {
            instance = new WifiStatusService();
        }
        return instance;
    }

    public void refreshStatus(@NonNull Context context) {
        status = doGetStatus(context);
        notifyListeners();
    }

    public WifiStatus getStatus() {
        return status;
    }

    private WifiStatus doGetStatus(@NonNull Context context) {
        WifiManager wifiManager = getWifiManager(context);
        if(!wifiManager.isWifiEnabled()) {
            return WifiStatus.NOT_ENABLED;
        }

        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        String ssid = wifiInfo.getSSID();

        if(EXPECTED_SSID_QUOTED.equals(ssid)) {
            return WifiStatus.CORRECT_NETWORK;
        }

        if(!QUOTED_SSID_PATTERN.matcher(ssid).matches()) {
            return WifiStatus.NOT_ENABLED;
        }

        return WifiStatus.WRONG_NETWORK;
    }

    private WifiManager getWifiManager(@NonNull Context context) {
        return (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
    }


}
