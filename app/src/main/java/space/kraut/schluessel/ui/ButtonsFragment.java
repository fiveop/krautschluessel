// Copyright (c) 2018, Philipp Matthias Schäfer (philipp.matthias.schaefer@posteo.de)
//
// This file is part of Krautschlüssel.
//
// Krautschlüssel is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Krautschlüssel is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with Kautschlüssel. If not, see <https://www.gnu.org/licenses/>.
package space.kraut.schluessel.ui;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import space.kraut.schluessel.R;
import space.kraut.schluessel.shared.AbstractChangeNotifyingService;
import space.kraut.schluessel.command.CommandService;
import space.kraut.schluessel.token.TokenService;
import space.kraut.schluessel.wifi.WifiStatusService;

public class ButtonsFragment extends Fragment implements AbstractChangeNotifyingService.ChangeListener {

    private View openFrontDoorButton;
    private View openButton;
    private View closeButton;

    private class OnClickListener implements View.OnClickListener {

        @NonNull
        private final CommandService.Command command;

        private OnClickListener(@NonNull CommandService.Command command) {
            this.command = command;
        }

        @Override
        public void onClick(View view) {
            FragmentActivity activity = getActivity();
            if(activity == null) {
                return;
            }

            commandService.sendCommand(activity, command);
        }

    }

    private CommandService commandService;
    private TokenService tokenService;
    private WifiStatusService wifiStatusService;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        tokenService = TokenService.getInstance();
        tokenService.addListener(this);

        wifiStatusService = WifiStatusService.getInstance();
        wifiStatusService.addListener(this);

        this.commandService = new CommandService(tokenService);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.buttons_fragment, container, false);

        openFrontDoorButton = view.findViewById(R.id.open_front_door_button);
        openFrontDoorButton.setOnClickListener(new OnClickListener(CommandService.Command.OUTDOOR_BUZZ));

        openButton = view.findViewById(R.id.open_button);
        openButton.setOnClickListener(new OnClickListener(CommandService.Command.INDOOR_OPEN));

        closeButton = view.findViewById(R.id.close_button);
        closeButton.setOnClickListener(new OnClickListener(CommandService.Command.INDOOR_LOCK));

        return view;
    }

    @Override
    public void notifyChanged() {
        FragmentActivity activity = getActivity();
        if(activity == null) {
            return;
        }

        Context context = activity.getApplicationContext();

        boolean enableButtons = tokenService.getToken(context) != null &&
                wifiStatusService.getStatus() == WifiStatusService.WifiStatus.CORRECT_NETWORK;

        if(getView() != null) {
            openFrontDoorButton.setEnabled(enableButtons);
            openButton.setEnabled(enableButtons);
            closeButton.setEnabled(enableButtons);
        }
    }

}
