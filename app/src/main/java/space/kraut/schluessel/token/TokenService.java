// Copyright (c) 2018, Philipp Matthias Schäfer (philipp.matthias.schaefer@posteo.de)
//
// This file is part of Krautschlüssel.
//
// Krautschlüssel is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Krautschlüssel is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with Kautschlüssel. If not, see <https://www.gnu.org/licenses/>.
package space.kraut.schluessel.token;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.regex.Pattern;

import space.kraut.schluessel.shared.AbstractChangeNotifyingService;

public class TokenService extends AbstractChangeNotifyingService {

    public static final int TOKEN_LENGTH = 78;

    private static final String TOKEN_PREFERENCE = "TOKEN PREFERENCE";
    private static final String PREFERENCE_FILENAME = "token_settings";

    private static final Pattern TOKEN_PATTERN = Pattern.compile("^[a-z0-9]{" + TOKEN_LENGTH + "}$");

    private static TokenService instance;

    private TokenService() {}

    public static TokenService getInstance() {
        if(instance == null) {
            instance = new TokenService();
        }
        return instance;
    }

    public @Nullable String getToken(@NonNull Context context) {
        SharedPreferences preferences = getSharedPreferences(context);
        return preferences.getString(TOKEN_PREFERENCE, null);
    }

    public void setToken(@NonNull Context context, @Nullable String token) {
        if(token == null || token.isEmpty() ) {
            token = null;
        } else if (!TOKEN_PATTERN.matcher(token).matches()) {
            throw new TokenError(TokenError.Type.WRONG_FORMAT);
        }

        SharedPreferences preferences = getSharedPreferences(context);
        preferences.edit().putString(TOKEN_PREFERENCE, token).apply();
        notifyListeners();
    }

    private SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(PREFERENCE_FILENAME, Context.MODE_PRIVATE);
    }

}
