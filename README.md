# Krautschlüssel

(There is an English version of README.md: [README_en.md](README_en.md))

Krautschlüssel ist eine Android-Anwendung, mit der man die Tür des Jenaer
Krautspaces auf- und zuschließen kann.

## Licenses

Die Anwendung wurde von Philipp Matthias Schäfer
(philipp.matthias.schaefer@posteo.de) geschrieben und unter GPL3-Lizenz
veröffentlicht. Der Lizenztext ist in der Datei [LICENSE](LICENSE) zu finden.

Krautschlüssel verwendet Icons von Googles Material Design Webseite:

https://material.io/tools/icons/

Die Icons sind von Google unter der Apache Lizenz, Version 2.0 veröffentlicht
worden. Der Lizenztext ist unter

https://www.apache.org/licenses/LICENSE-2.0.html

zu finden.

Krautschlüssel verwendet mehrere Bibliotheken. Diese wurden von ihren
Urheberrechtsinhabern unter der Apache Lizenz, Version 2.0 veröffentlicht. Der
Lizenztext ist unter

https://www.apache.org/licenses/LICENSE-2.0.html

zu finden.

Es folgt eine Liste dieser Bibliotheken mit Verweisen zu ihrem Quellcode.

* Barcode Scanner View/ZXing Scanner View:
  https://github.com/dm77/barcodescanner

* ZXing
  https://github.com/zxing/zxing

* AOSP Support Library/Android Lifecycle Runtime/Android Arch-Common
  https://android.googlesource.com/platform/frameworks/support
