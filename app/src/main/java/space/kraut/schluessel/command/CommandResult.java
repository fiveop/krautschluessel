// Copyright (c) 2018, Philipp Matthias Schäfer (philipp.matthias.schaefer@posteo.de)
//
// This file is part of Krautschlüssel.
//
// Krautschlüssel is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Krautschlüssel is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with Kautschlüssel. If not, see <https://www.gnu.org/licenses/>.
package space.kraut.schluessel.command;

public enum CommandResult {

        CONNECTION_ERROR,
        ERROR_RESPONSE,
        SUCCESS

}
