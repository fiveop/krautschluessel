// Copyright (c) 2018, Philipp Matthias Schäfer (philipp.matthias.schaefer@posteo.de)
//
// This file is part of Krautschlüssel.
//
// Krautschlüssel is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Krautschlüssel is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with Kautschlüssel. If not, see <https://www.gnu.org/licenses/>.
package space.kraut.schluessel.token;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;
import java.nio.charset.MalformedInputException;
import java.nio.charset.UnmappableCharacterException;
import java.nio.charset.UnsupportedCharsetException;

public class TokenReadService {


    private final TokenService tokenService;

    public TokenReadService(TokenService tokenService) {
        this.tokenService = tokenService;
    }

    public void readAndSetToken(Context context, Uri fileUri) {
        String token = readTokenFromFile(context, fileUri);
        tokenService.setToken(context, token);
    }

    private String readTokenFromFile(Context context, Uri fileUri) {
        ContentResolver resolver = context.getContentResolver();
        InputStream stream;
        try {
            stream = resolver.openInputStream(fileUri);
        } catch (FileNotFoundException e) {
            throw new TokenError(TokenError.Type.FILE_NOT_FOUND);
        }
        if(stream == null) {
            throw new TokenError(TokenError.Type.FILE_NOT_FOUND);
        }
        Charset charset;
        try {
            charset = Charset.forName("UTF-8");
        } catch(UnsupportedCharsetException e) {
            throw new TokenError(TokenError.Type.UNSUPPORTED_ENCODING);
        }
        CharsetDecoder decoder = charset.newDecoder();
        decoder.onMalformedInput(CodingErrorAction.REPORT);
        decoder.onUnmappableCharacter(CodingErrorAction.REPORT);

        BufferedReader reader = new BufferedReader(new InputStreamReader(stream, decoder));
        char[] buffer = new char[TokenService.TOKEN_LENGTH];
        int charactersRead;
        try {
            try {
                charactersRead = reader.read(buffer);
            } catch (MalformedInputException | UnmappableCharacterException e) {
                throw new TokenError(TokenError.Type.WRONG_ENCODING);
            }
            if (charactersRead != 78) {
                throw new TokenError(TokenError.Type.WRONG_FORMAT);
            }
            if (reader.read() != -1) {
                throw new TokenError(TokenError.Type.WRONG_FORMAT);
            }
        } catch (IOException e) {
            throw new TokenError(TokenError.Type.IO_PROBLEM);
        }
        return new String(buffer);
    }

}
