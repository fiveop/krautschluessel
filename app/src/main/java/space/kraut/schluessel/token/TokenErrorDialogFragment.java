// Copyright (c) 2018, Philipp Matthias Schäfer (philipp.matthias.schaefer@posteo.de)
//
// This file is part of Krautschlüssel.
//
// Krautschlüssel is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Krautschlüssel is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with Kautschlüssel. If not, see <https://www.gnu.org/licenses/>.
package space.kraut.schluessel.token;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

import space.kraut.schluessel.R;
import space.kraut.schluessel.shared.AbstractErrorDialogFragment;

public class TokenErrorDialogFragment extends AbstractErrorDialogFragment {

    private static final String TOKEN_ERROR_TYPE_ARGUMENT = "space.kraut.schluessel.fragments.TOKEN_ERROR_TYPE_ARGUMENT";

    public static void open(FragmentActivity activity, TokenError error) {
        Bundle arguments = new Bundle();
        arguments.putSerializable(TokenErrorDialogFragment.TOKEN_ERROR_TYPE_ARGUMENT, error.getType());

        DialogFragment fragment = new TokenErrorDialogFragment();
        fragment.setArguments(arguments);

        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        fragment.show(fragmentManager, null);
    }

    @Override
    protected int getTitleId() {
        return R.string.token_error_dialog_title;
    }

    @Override
    protected int getMessageId() {
        TokenError.Type type = (TokenError.Type) getArguments().getSerializable(TOKEN_ERROR_TYPE_ARGUMENT);
        switch(type) {
            case FILE_NOT_FOUND:
                return R.string.token_error_file_not_found_message;
            case IO_PROBLEM:
                return R.string.token_error_io_problem_message;
            case UNSUPPORTED_ENCODING:
                return R.string.token_error_unsupported_encoding_message;
            case WRONG_ENCODING:
                return R.string.token_error_wrong_encoding_message;
            case WRONG_FORMAT:
                return R.string.token_error_wrong_format_message;
        }
        throw new RuntimeException(getContext().getString(R.string.unexpected_error_message));
    }

}
