// Copyright (c) 2018, Philipp Matthias Schäfer (philipp.matthias.schaefer@posteo.de)
//
// This file is part of Krautschlüssel.
//
// Krautschlüssel is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Krautschlüssel is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with Kautschlüssel. If not, see <https://www.gnu.org/licenses/>.
package space.kraut.schluessel.token;


import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

import space.kraut.schluessel.R;
import space.kraut.schluessel.token.TokenError;
import space.kraut.schluessel.token.TokenErrorDialogFragment;
import space.kraut.schluessel.token.TokenService;

public class EnterTokenDialogFragment extends DialogFragment implements DialogInterface.OnClickListener {

    private EditText tokenEditText;
    private TokenService tokenService;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        tokenService = TokenService.getInstance();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Context context = getActivity();

        View dialogView = View.inflate(context, R.layout.enter_token_dialog, null);

        tokenEditText = dialogView.findViewById(R.id.tokenEditText);

        AlertDialog dialog = new AlertDialog
                .Builder(context)
                .setTitle(R.string.enter_token_dialog_title)
                .setPositiveButton(R.string.save, this)
                .setNegativeButton(R.string.cancel, this)
                .setView(dialogView)
                .create();

        Window window = dialog.getWindow();
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        return dialog;
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int which) {
        if(DialogInterface.BUTTON_POSITIVE == which) {
            FragmentActivity activity = getActivity();
            if(activity == null) {
                return;
            }

            Context context = activity.getApplicationContext();

            try {
                tokenService.setToken(context, tokenEditText.getText().toString());
            } catch (TokenError error) {
                TokenErrorDialogFragment.open(activity, error);
            }
        }

    }

}
