// Copyright (c) 2018, Philipp Matthias Schäfer (philipp.matthias.schaefer@posteo.de)
//
// This file is part of Krautschlüssel.
//
// Krautschlüssel is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Krautschlüssel is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with Kautschlüssel. If not, see <https://www.gnu.org/licenses/>.
package space.kraut.schluessel.command;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

import space.kraut.schluessel.R;
import space.kraut.schluessel.token.TokenErrorDialogFragment;
import space.kraut.schluessel.shared.AbstractErrorDialogFragment;

public class CommandErrorDialogFragment extends AbstractErrorDialogFragment {

    private static final String COMMAND_RESULT_ARGUMENT = "space.kraut.schluessel.fragments.COMMAND_RESULT_ARGUMENT";

    public static void open(FragmentActivity activity, CommandResult result) {
        Bundle arguments = new Bundle();
        arguments.putSerializable(CommandErrorDialogFragment.COMMAND_RESULT_ARGUMENT, result);

        DialogFragment fragment = new CommandErrorDialogFragment();
        fragment.setArguments(arguments);

        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        fragment.show(fragmentManager, null);
    }


    @Override
    protected int getTitleId() {
        return R.string.command_error_dialog_title;
    }

    @Override
    protected int getMessageId() {
        CommandResult result = (CommandResult) getArguments().getSerializable(COMMAND_RESULT_ARGUMENT);
        switch(result) {
            case CONNECTION_ERROR:
                return R.string.command_error_connection_error_message;
            case ERROR_RESPONSE:
                return R.string.command_error_error_response_message;
            case SUCCESS:
        }
        throw new RuntimeException(getContext().getString(R.string.unexpected_error_message));
    }
}
