package space.kraut.schluessel.about;

import android.os.Bundle;
import android.app.Activity;
import android.support.v7.app.AppCompatActivity;

import space.kraut.schluessel.R;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_activity);
    }
}
